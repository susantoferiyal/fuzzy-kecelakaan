<?php
error_reporting(E_ALL);
error_reporting();
ini_set('display_errors', 1);
ini_set('max_execution_time', 60 * 10);
ini_set('memory_limit', '256M');

function get_paging($modul, $total, $per_page){
    $CI =&get_instance();
    
    $config['base_url'] = site_url("$modul?");
    $config['total_rows'] = $total;
    $config['per_page'] = $per_page;
    $config['page_query_string'] = TRUE;
    $config['reuse_query_string'] = TRUE;
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a>';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $CI->pagination->initialize($config);
    
    return '<ul class="pagination pagination-sm  pull-right">'.$CI->pagination->create_links().'</ul>';
}
function set_msg($msg, $type='success'){
    $CI =& get_instance();
    $CI->session->set_userdata('MSG', array('msg' => $msg, 'type' => $type));    
}
function show_msg(){
    $CI =& get_instance();
    $MSG = $CI->session->userdata('MSG');
    if($MSG){
        $CI->session->set_userdata('MSG', array());
        return '<div class="alert alert-'. $MSG['type'].'" alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>'.$MSG['msg'].'</div>';
    }    
}

function get_match($needed, $data){
    $matches = 0;
    foreach($data as $k => $v){
        $counter = 0;            
        foreach($v as $a => $b){
            if(in_array($b, $needed))
                $counter++;
        }
        if($counter >= count($needed))
            $matches++;
    }
    return $matches;
}

function get_com_category($arr_kategory, $itemset){  
    return getCombinations($arr_kategory, $itemset)      ;    
}

function getCombinations($base,$n){
    $baselen = count($base);
    if($baselen == 0){
        return;
    }
    if($n == 1){
        $return = array();
        foreach($base as $b){
            $return[] = array($b);
        }
        return $return;
    }else{
        //get one level lower combinations
        $oneLevelLower = getCombinations($base,$n-1);

        //echo '<pre>'.$n.': '.print_r($oneLevelLower, 1).'</pre>';

        //for every one level lower combinations add one element to them that the last element of a combination is preceeded by the element which follows it in base array if there is none, does not add
        $newCombs = array();
        foreach((array)$oneLevelLower as $oll){
            $lastEl = $oll[$n-2];
            $found = false;
            foreach($base as  $key => $b){
                if($b == $lastEl){
                    $found = true;
                    continue;
                    //last element found
                }
                if($found == true){
                    //add to combinations with last element
                    if($key < $baselen){
                        $tmp = $oll;
                        $newCombination = array_slice($tmp,0);
                        $newCombination[]=$b;
                        $newCombs[] = array_slice($newCombination,0);
                    }
                }
            }
        }
    }
    return $newCombs;
}

function get_candidate($data, $com_category){
    $arr = array();    
    foreach($com_category as $key => $val){        
        $arr[$key] = get_match($val, $data);
    }
    return $arr;    
}
function get_fuzzy($candicate, $min, $max){
    $arr = array();
    foreach($candicate as $key => $val){
        $arr[$key] = groupFuzzy($val, $min, $max);
    }
    return $arr;
}
function get_support($candicate, $total){
    $arr = array();
    foreach($candicate as $key => $val){
        $arr[$key] = $val / $total;
    }
    return $arr;
}
function filter_candicate($candicate, $support, $min_support, $fuzzySet){
    $arr = array();
    foreach($candicate as $key => $val){
        if($support[$key]>=$min_support
            || filter_fuzzy($fuzzySet[$key], $min_support, min($candicate), max($candicate))
            ) {
            $arr[$key] = $val;
        }
    }
    return $arr;
}

function filter_category($l_can, $com_category){
    $arr = array();
    foreach($l_can as $key => $val){
        foreach($com_category[$key] as $v){
            $arr[$v] = $v;
        }
    }
    //echo '<pre>'. print_r($arr, 1). '</pre>';
    return array_values($arr);
}

function ass_key($large_candicate, $com_category){
    
    $category = filter_category($large_candicate, $com_category);
    
    $arr = array();
    foreach($large_candicate as $key => $val){
        $arr[$key] = $com_category[$key];
    }
    
    $arr2 = array();
    $no = 0;
    foreach($arr as $key => $val){
        for($a = 0; $a < count($val); $a++){                                       
            $arr2[$no]['left'] = $val;
            $arr2[$no]['right'] = $val[$a];
            unset($arr2[$no]['left'][$a]); 
               
            $arr2[$no]['index'] = $key;
            $no++;
        }                
    }
    
    $arr = $arr2;
    
    return $arr;
}
/*
function get_ass_name($keys, $KATEGORI){
    $arr = array();
    foreach($keys as $key){
        $arr[] = $KATEGORI[$key]->nama_kategori;
    }
    return implode(', ', $arr);
}*/

function get_ass_qty($ass_key, $data){
    //echo '<pre>' . print_r($ass_key, 1) . '</pre>';

    $arr = array();
    foreach($ass_key as $key => $val){
        //$arr[$key] = get_match(array($val['right']), $data);
        $arr[$key] = get_match($val['left'], $data);
    }
    return $arr;
}

function get_confidence($ass_key, $ass_qty, $large_candicate){
    $arr = array();
    foreach($ass_key as $key => $val){
        $arr[$key] = $large_candicate[$val['index']] / $ass_qty[$key];
    }
    return $arr;
}

function convert_data($data, $kategori){
    $CI =&get_instance();
    $arr = array();
    $arr_kategori = array();
    foreach($kategori as $row){
        $arr_kategori[] = strtolower($row->nama_kategori);
    }
    //echo '<pre>' . print_r($arr_kategori, 1) . '</pre>';
    foreach($data as $row){
        $arr[$row->id_data] = array();
        foreach($arr_kategori as $key => $val){
            $nilai = 0;
            if(substr($row->nama_data, $key, 1)==1){
                $arr[$row->id_data][] = $val;
                $nilai = 1;
            }
            //$key = $key + 1;
            //$CI->db->query("REPLACE INTO tb_relasi (id_data, id_kategori, nilai) VALUES ('$row->id_data', '$key', '$nilai')");
        }        
    }              
    //echo '<pre>' . print_r($arr, 1) . '</pre>';  
    return $arr;
}





















function load_view($view, $data = array())
{
    $CI =&get_instance();
    $CI->load->view('header', $data);
    $CI->load->view($view, $data);
    $CI->load->view('footer', $data);    
}

function load_view_cetak($view, $data = array())
{
    $CI =&get_instance();
    $CI->load->view('header_cetak', $data);
    $CI->load->view($view, $data);
    $CI->load->view('footer_cetak', $data);    
}

function load_message($message = '', $type = 'danger')
{
    if($type=='danger') 
    {
        $data['title'] = 'Error';
    }
    else 
    {
        $data['title'] = 'Success';
    }
    
    $data['class'] = $type;
    $data['message'] = $message;
    
    load_view('message', $data);
    
}

function refresh_stok($kode_barang){
    $CI =& get_instance();
    
    $row = $CI->db->query("SELECT SUM(jumlah) AS tambah FROM tb_stok WHERE kode_barang='$kode_barang'")->row();
    $tambah = $row->tambah * 1;
    $row = $CI->db->query("SELECT SUM(jumlah) AS kurang FROM tb_barang_keluar WHERE kode_barang='$kode_barang'")->row();
    $kurang = $row->kurang * 1;
    
    $CI->db->query("UPDATE tb_barang SET stok=$tambah-$kurang
    WHERE kode_barang='$kode_barang'");
    
}

function format_date($datetime, $format='d M Y'){
    $date = date_create($datetime);
    return date_format($date, $format);
}


function get_level_radio($selected){
    $arr = array('Admin' => 'Admin', 'BA' => 'Beauty Advisor');
    $a = '';
    foreach($arr as $key => $val){
        if($key==$selected)
            $a.="<label class='radio-inline'>
                  <input type='radio' name='level' value='$key' checked> $val
                </label>";
        else
            $a.="<label class='radio-inline'>
                  <input type='radio' name='level' value='$key'> $val
                </label>";    
    }
    return '<div class="radio">'.$a.'</div>';
} 

function get_kategori_option($selected = ''){
    $CI =& get_instance();    
    $rows = $CI->kategori_model->tampil();
    
    $a = '';
    foreach($rows as $row){
        if($selected==$row->id_kategori)
            $a.="<option value='$row->id_kategori' selected>$row->nama_kategori</option>";
        else
            $a.= "<option value='$row->id_kategori'>$row->nama_kategori</option>";
    }
    return $a;
}

function get_karyawan_option($selected = ''){
    $CI =& get_instance();    
    $rows = $CI->karyawan_model->tampil();
    
    $a = '';
    foreach($rows as $row){
        if($selected==$row->kode_karyawan)
            $a.="<option value='$row->kode_karyawan' data-jabatan='$row->nama_jabatan' data-nama_karyawan='$row->nama_karyawan' data-gaji_per_hari='$row->gaji_per_hari' selected>$row->nama_karyawan</option>";
        else
            $a.= "<option value='$row->kode_karyawan'  data-jabatan='$row->nama_jabatan' data-nama_karyawan='$row->nama_karyawan' data-gaji_per_hari='$row->gaji_per_hari'>$row->nama_karyawan</option>";
    }
    return $a;
}

function get_ba_option($selected = ''){
    $CI =& get_instance();    
    $rows = $CI->ba_model->tampil();
    
    $a = '';
    foreach($rows as $row){
        if($selected==$row->id_ba)
            $a.="<option value='$row->id_ba' selected>$row->nama_ba</option>";
        else
            $a.= "<option value='$row->id_ba'>$row->nama_ba</option>";
    }
    return $a;
}

function if_null($value, $default){
    if(isset($value) && $value)
        return $value;    
    return $default    ;
}

function calculate_stok($kode_barang){
    $CI =& get_instance();    
    $barang_keluar = $CI->db->query("SELECT SUM(jumlah) AS total FROM tb_barang_keluar WHERE kode_barang='$kode_barang'")->row()->total;
    $barang_keluar = $CI->db->query("SELECT SUM(jumlah) AS total FROM tb_barang_keluar WHERE kode_barang='$kode_barang'")->row()->total;
    $beli_retur = $CI->db->query("SELECT SUM(jumlah) AS total FROM tb_beli_retur WHERE kode_barang='$kode_barang'")->row()->total;
    $jual_retur = $CI->db->query("SELECT SUM(jumlah) AS total FROM tb_jual_retur WHERE kode_barang='$kode_barang'")->row()->total;
    $stok = $barang_keluar - $barang_keluar - $beli_retur + $jual_retur;
    $stok*=1;
    $CI->db->query("UPDATE tb_barang SET stok=$stok WHERE kode_barang='$kode_barang'");
    return $stok;
}

function print_error(){
    return validation_errors('<div class="alert alert-danger" alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>','</div>');
}

function print_msg($msg = '', $type = 'danger'){
    echo '<div class="alert alert-'.$type.'" alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$msg .'</div>';
}

function kode_oto($field, $table, $prefix, $length){
    $CI =& get_instance();
    $query = $CI->db->query("SELECT $field AS kode FROM $table WHERE $field REGEXP '{$prefix}[0-9]{{$length}}' ORDER BY $field DESC");    
    $row = $query->row_object();    
    
    if($row){
        return $prefix . substr( str_repeat('0', $length) . (substr($row->kode, - $length) + 1), - $length );
    } else {
        return $prefix . str_repeat('0', $length - 1) . 1;
    }
}

function groupFuzzy($value, $min, $max) {
    $range = ($max - $min)/5;

    $sangat_rendah = array($min, $min + $range );
    $rendah = array(max($sangat_rendah), max($sangat_rendah)+$range);
    $medium = array(max($rendah), max($rendah)+$range);
    $tinggi = array(max($medium), max($medium)+$range);
    $sangat_tinggi = array(max($tinggi), $max);

    $fuzzy_sr = array(min($sangat_rendah), max($rendah)-(max($rendah)-min($rendah))/5);
    $fuzzy_r = array(max($sangat_rendah)-(max($sangat_rendah)-min($sangat_rendah))/5, max($medium)-(max($medium)-min($medium))/5);
    $fuzzy_m = array(max($rendah)-(max($rendah)-min($rendah))/5, max($tinggi)-(max($tinggi)-min($tinggi))/5);
    $fuzzy_t = array(max($medium)-(max($medium)-min($medium))/5, max($rendah)-(max($rendah)-min($rendah))/5);
    $fuzzy_st = array(max($tinggi)-(max($tinggi)-min($tinggi))/5, $max);

    // $x = $value;
    // $a = min($sangat_rendah);
    // $b = min($fuzzy_sr);
    // $c = max($sangat_rendah);
    // $d = max($fuzzy_sr);

    // print_r(($x - $a).'-');
    // print_r($b.'-'.$a);
    // print_r(($d - $x).'-');
    // print_r(($d - $c).'-');

    // $membershipArray = [];
    // $membershipValue = max(min(($x - $a) / ($b - $a), 1, ($d - $x) / ($d - $c)), 0);
    // print_r('$membershipValue '.$membershipValue);

    $range = '';
    if ($value >= $fuzzy_sr[0] && $value <= $fuzzy_sr[1]) {
        $range .= 'very low';
    } 

    if ($value >= $fuzzy_r[0] && $value <= $fuzzy_r[1]) {
        if ($range != '')
            $range .= ',';
        $range .= 'low';
    }

    if ($value >= $fuzzy_m[0] && $value <= $fuzzy_m[1]) {
        if ($range != '')
            $range .= ',';
        $range .= 'medium';
    }

    if ($value >= $fuzzy_t[0] && $value <= $fuzzy_t[1]) {
        if ($range != '')
            $range .= ',';
        $range .= 'high';
    }

    if ($value >= $fuzzy_st[0] && $value <= $fuzzy_st[1]) {
        if ($range != '')
            $range .= ',';
        $range .= 'very high';
    }

    return $range;

    
}

function filter_fuzzy($value, $minValue, $min, $max) {
    $pass = false;
    $minGroup = groupFuzzy($minValue, $min, $max);
    $keys = explode(',', $value);
        $minKeys = explode(',', $minGroup);
        foreach ($keys as $key) {
            foreach ($minKeys as $minKey) {
                if ($key == $minKey) {
                    $pass = true;
                    break;
                }
            }
        }
    return $pass;
}
function fuzzyfikasi($value){
    $max = max($value);
    $min = min($value);
    $range = ($max - $min)/5;


    //print_r($sangat_rendah) ;
     echo "Range = ".$range."<br>";
    

     $sangat_rendah = array($min, $min + $range );
     $rendah = array(max($sangat_rendah), max($sangat_rendah)+$range);
     $medium = array(max($rendah), max($rendah)+$range);
     $tinggi = array(max($medium), max($medium)+$range);
     $sangat_tinggi = array(max($tinggi), max($data));

     $fuzzy_sr = array(min($sangat_rendah), max($rendah)-(max($rendah)-min($rendah))/5);

    $fuzzy_r = array(max($sangat_rendah)-(max($sangat_rendah)-min($sangat_rendah))/5, max($medium)-(max($medium)-min($medium))/5);

    $fuzzy_m = array(max($rendah)-(max($rendah)-min($rendah))/5, max($tinggi)-(max($tinggi)-min($tinggi))/5);

    $fuzzy_t = array(max($medium)-(max($medium)-min($medium))/5, max($rendah)-(max($rendah)-min($rendah))/5);

    $fuzzy_st = array(max($tinggi)-(max($tinggi)-min($tinggi))/5, max($data));
}