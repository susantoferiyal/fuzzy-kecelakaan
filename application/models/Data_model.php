<?php
class Data_model extends CI_Model {
    
    protected $table = 'data';

    protected $kode = 'id_laka';
    
    public function tampil( $search='', $count = false, $limit = 18446744073709551615, $offset = 0)
    {                        
        $this->db->like( 'kendaraan', $search );
                                
        if($count){
            $this->db->from($this->table);
            return $this->db->count_all_results();
        } else {
            $this->db->order_by( 'id_laka', 'ASC' );
            return $this->db->get($this->table, $limit, $offset)->result();
        }   
    }
    
    public function get_relasi() 
    {
        $rows = $this->db->query("SELECT id_laka,kendaraan as atribut FROM data
UNION ALL
SELECT id_laka,usia FROM data
UNION ALL
SELECT id_laka,jenis_kelamin FROM data
UNION ALL
SELECT id_laka,sim FROM data
UNION ALL
SELECT id_laka,faktor_pengemudi FROM data
UNION ALL
SELECT id_laka,profesi FROM data
UNION ALL
SELECT id_laka,status FROM data 
ORDER BY id_laka ASC")->result();

        $data = array();
        foreach($rows as $row){
            $data[$row->id_laka][$row->atribut] = $row->atribut;
        }
        return $data;
    }
    public function tambah($fields)
    {
        $this->db->insert($this->table, $fields);         
    }      
}