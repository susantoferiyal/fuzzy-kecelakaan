<?php
class Data extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        if(!$this->session->userdata('login'))
            redirect('user/login');
        
        $this->load->model('data_model');   
    }

    public function index()
    {      
        $limit = 5;
        
        $data['title'] = 'DATA LAKA'; 
        $data['offset'] = $this->input->get('per_page');        
        $data['rows'] = $this->data_model->tampil($this->input->get('search'), false, $limit, $data['offset']);
        $data['total_rows'] =   $this->data_model->tampil($this->input->get('search'), true );
                           
        $data['paging'] = get_paging('data', $data['total_rows'], $limit);
        load_view('data', $data);
    }

    public function import(){
        if($this->input->POST('simpan')){

            $data = array();

            $config['allowed_types'] = 'csv';
            $config['max_size']  = '2048';


            $this->load->helper('file');
            $this->load->library('upload', $config);
            $this->load->library('csvimport', $config);

            $lokasi = $config['upload_path'] = 'assets/csv/'; 
            
            $this->upload->initialize($config);
            if ( ! $this->upload->do_upload('userfile')){
                set_msg('Gagal upload file', 'danger');
            }else{
                $uploadData = $this->upload->data();
                $filename = $lokasi.$uploadData['file_name'];
                $csv_array = $this->csvimport->get_array($filename);
                
                if(!empty($csv_array)){    
                    $kategori = array();                
                    foreach ($csv_array as $row){   
                       // if ($row['Bagian'] != "Kesuplayeran") {                            
                            //$date = date_create($row['Tanggal'], timezone_open("Europe/Oslo"));
                            $row_data = array(
                                'id_laka'=>$row['id_laka'],
                                'tanggal_waktu'=>$row['tanggal_waktu'],
                                'kendaraan'=>$row['kendaraan'],
                                'usia'=>$row['usia'],
                                'jenis_kelamin'=>$row['jenis_kelamin'],
                                'sim'=>$row['sim'],
                                'faktor_jalan'=>$row['faktor_jalan'],
                                'faktor_pengemudi'=>$row['faktor_pengemudi'],
                                'pengguna_helm'=>$row['pengguna_helm'],
                                'profesi'=>$row['profesi'],
                                'status'=>$row['status']

                            );                       
                            $this->data_model->tambah($row_data);                                             
                        }
                    }                
                    set_msg("Data Dari <b>$filename</b> Berhasil Disimpan Di Database", 'success');
                } 
            
        }
        $data['title'] = 'Import Data';
        load_view('data_import', $data);
    }
}