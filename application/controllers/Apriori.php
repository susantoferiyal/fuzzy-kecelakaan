<?php
class Apriori extends CI_Controller {

    public function __construct()
    {
        parent::__construct();   
        
        if(!$this->session->userdata('login'))
            redirect('user/login');
            
        $this->load->model('data_model'); 
    }

    public function index()
    {           
        $data['title'] = 'Perhitungan <i>Fuzzy Association Rules</i>';
        
        $this->form_validation->set_rules( 'min_support', 'Minimal Support', 'required' );
        $this->form_validation->set_rules( 'min_confidence', 'Minimal Confidence', 'required' );
                
        // $row = $this->db->query("SELECT MIN(tanggal) AS tanggal_awal, MAX(tanggal) AS tanggal_akhir FROM tb_data")->row();
        // $data['tanggal_awal'] = set_value('tanggal_awal', $row->tanggal_awal);
        // $data['tanggal_akhir'] = set_value('tanggal_akhir', $row->tanggal_akhir);
                                
        if ($this->form_validation->run() === FALSE)
        {
            load_view('apriori', $data);     
        }
        else
        {   
            $data['rows'] =  $this->data_model->tampil($this->input->get('search'));
            $data['data'] = $this->data_model->get_relasi();    
            $this->load->view('header', $data);
            $this->load->view('apriori');
            $this->load->view('apriori_hasil');
            $this->load->view('footer');
        }     
    }        
}