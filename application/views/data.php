<?php
/*//$this->db->query("TRUNCATE tb_data");
//$this->db->query("TRUNCATE tb_kategori");
$this->db->query("TRUNCATE tb_relasi");

//$rows = $this->db->query("SELECT tanggal, uraian1 FROM import ORDER BY tanggal, uraian1 LIMIT 2569")->result();
$rows = $this->db->query("SELECT tanggal, uraian1 FROM import ORDER BY tanggal, uraian1 LIMIT 6000")->result();
$tanggal = array();
$kategori = array();
foreach($rows as $row){
    $data[$row->tanggal] = $row->tanggal;
    $kategori[$row->uraian1] = $row->uraian1;
    $relasi[$row->tanggal][$row->uraian1] = 1;
}

$i = 1;
foreach($data as $key => $val){
    //$this->db->query("INSERT INTO tb_data VALUES(null, '$key')");
    $j = 1;
    foreach($kategori as $k => $v){
        // if($i==1)
        //     $this->db->query("INSERT INTO tb_kategori VALUES(null, 'K$j', '$k')");  

        //$nilai = in_array($k, array_keys($relasi[$key])) ? 1 : 0;

        if(in_array($k, array_keys($relasi[$key])))
            $this->db->query("INSERT INTO tb_relasi VALUES (null, '$i', '$j', '1')");
        $j++; 
    }
    $i++;
}
echo '<pre>' . print_r($data, 1) . '</pre>';
die();*/
?>
<?=show_msg()?>
<div class="panel panel-default">
    <div class="panel-heading">
        <form class="form-inline">
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Pencarian. . ." name="search" value="<?=$this->input->get('search')?>" />
            </div>
            <div class="form-group">
                <button class="btn btn-success"><i class="glyphicon glyphicon-refresh"></i> Refresh</a>
            </div>
            <div class="form-group">
                <a class="btn btn-info" href="<?=site_url('data/import')?>"><i class="glyphicon glyphicon-import"></i> Import</a>
            </div>
        </form>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
        <thead>
            <tr>
                <th>id LAKA</th>
                <th>tanggal_waktu</th>
                <th>kendaraan</th>
                <th>usia</th>
                <th>jenis_kelamin</th>
                <th>sim</th>
                <th>faktor jalan</th>
                <th>faktor pengemudi</th>
                <th>pengguna helm</th>
                <th>profesi</th>
                <th>status</th>
            </tr>
        </thead>
        <?php    
        $no=$offset;
        foreach($rows as $row):?>
        <tr>
            <td><?=$row->id_laka?></td>
            <td><?=$row->tanggal_waktu?></td>
            <td><?=$row->kendaraan?></td>
            <td><?=$row->usia?></td>
            <td><?=$row->jenis_kelamin?></td>
            <td><?=$row->sim?></td>
            <td><?=$row->faktor_jalan?></td>
            <td><?=$row->faktor_pengemudi?></td>
            <td><?=$row->pengguna_helm?></td>
            <td><?=$row->profesi?></td>
            <td><?=$row->status?></td>
        </tr>
        <?php endforeach;?>
        </table>
    </div>
    <div class="panel-footer clearfix">
        <span class="pull-left">Menampilkan <?=count($rows)?> dari <?=$total_rows?> data</span>
        <?=$paging?>
    </div>
</div>