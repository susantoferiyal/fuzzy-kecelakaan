<form class="form-horizontal" method="post">
    <div class="form-group">
        <label class="col-md-3 control-label">Min support <span class="text-danger">*</span></label>
        <div class="col-md-6">
            <div class="input-group">
                <input class="form-control" type="number" name="min_support" value="<?=set_value('min_support', 50)?>" />
                <span class="input-group-addon">%</span>
            </div>            
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Min confidence <span class="text-danger">*</span></label>
        <div class="col-md-6">
            <div class="input-group">
                <input class="form-control" type="number" name="min_confidence" value="<?=set_value('min_confidence', 75)?>" />
                <span class="input-group-addon">%</span>
            </div>            
        </div>
    </div>
    <div class="form-group text-center">
        <button class="btn btn-second"><span class="glyphicon glyphicon-signal"></span> Proses</button>
    </div>
</form>


