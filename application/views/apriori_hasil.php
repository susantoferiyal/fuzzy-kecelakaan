
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// $tanggal_awal = set_value('tanggal_awal', $tanggal_awal);
// $tanggal_akhir = set_value('tanggal_akhir', $tanggal_akhir);

$min_support = set_value('min_support', 25);
$min_confidence = set_value('min_confidence', 25);

$kategori = array();
foreach($data as $key => $val){
    foreach($val as $k => $v){
        $kategori[$v] = $v;
    }
}


$arr_kategory = array_values($kategori);
?>
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: '#fefefe';
    margin: auto;
    padding: 20px;
    border: 1px solid '#888';
    width: 80%;
}

/* The Close Button */
.close {
    color: '#aaaaaa';
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: '#000';
    text-decoration: none;
    cursor: pointer;
}
</style>
<div class="form-group text-center">
<meta name="viewport" content="width=device-width, initial-scale=1">

<div class="panel panel-default">

    <div class="panel-heading">
        <h3 class="panel-title">Itemset</h3>
    </div>
    <div class="panel-body" style="overflow: auto">

        <table class="table table-bordered table-hover table-striped datatable">
            <thead>
                <tr>
                   
                    <th>id</th>
                    <th>Data</th>
                </tr>
            </thead>
            <?php    
           
            foreach($data as $key => $val):?>
            <tr>
               
                <td class="nw"><?=$key?></td>
                <td><?=implode(', ',array_keys($val))?></td>
            </tr>
            <?php endforeach;?>
        </table>         
    </div>
</div>
<?php 
$itemset = 1;
$stop = false;
$large_candicate = array();
$com_category = array();
$support = array();
$fuzzy = array();

// echo '<pre>' . print_r($data, 1) . '</pre>';

// print_r('data cok: '.$data);

while(!$stop):
    $temp_large_candidate = $large_candicate;
    $temp_com_category = $com_category;
    $temp_support = $support;
        
    $com_category = get_com_category($arr_kategory, $itemset);
    $qty = get_candidate($data, $com_category);

    $min = min($qty);
    $max = max($qty);            
    
    $support = get_support($qty, count($data));
    $fuzzy = get_fuzzy($qty, $min, $max,count($data));

    $large_candicate = filter_candicate($qty, $support, $min_support / 100, $fuzzy); 

    //echo '<pre>' . print_r($qty, 1) . '</pre>';

    if($large_candicate){?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><a data-toggle="collapse" href="#CC_<?=$itemset?>">C<?=$itemset?> (Kandidat <?=$itemset?>-itemset)</a></h3>
        </div>
        <div class="table-responsive collapse" id="CC_<?=$itemset?>">



            <table class="table table-bordered table-striped">
                <thead><tr>
                  <!-- <th>No</th> -->
                    <?php for($a = 1; $a <= $itemset; $a++):?>
                    <th>Kategori</th>
                    <?php endfor?>
                    <th>Qty</th>
                    <th>Support</th>
                    <!-- <th>Support</th> -->
                    <th>Fuzzy Set</th>
                </tr></thead>
                <?php 
                $no = 1;                                                                       
                foreach($com_category as $key => $val):?>
                <tbody>
                <tr>
                <!-- <td><?=$no++?></td>  -->
                    <?php foreach($val as $k):?>
                    <th><?=$k?></th>
                    <?php endforeach?>
                    <td><?=$qty[$key]?></td>
                    <td><?=round($support[$key] * 100, 2)?></td>
                    <!-- <td><?=round($support[$key], 2)?></td> -->
                    <td><?=$fuzzy[$key]?></td>
                </tr>
                </tbody>
                <?php endforeach?>
            </table>            
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><a data-toggle="collapse" href="#LC_<?=$itemset?>">L<?=$itemset?> (Large <?=$itemset?>-itemset)</a></h3>
        </div>
        <div class="table-responsive collapse" id="LC_<?=$itemset?>">
            <table class="table table-bordered table-striped">
                <thead><tr>
                    <th>No</th>
                    <?php for($a = 1; $a <= $itemset; $a++):?>
                    <th>Kategori<?=$a?></th>
                    <?php endfor?>
                    <th>Qty</th>
                    <th>Support</th>
                    <th>Fuzzy Set</th>
                </tr></thead>
                <?php 
                $no = 1;                       
                foreach($large_candicate as $key => $val):?>
                <tr>
                    <td><?=$no++?></td>
                    <?php foreach($com_category[$key] as $k):?>
                    <td><?=$k?></td>
                    <?php endforeach?>
                    <td><?=$large_candicate[$key]?></td>
                    <td><?=round($support[$key] * 100, 2)?></td>
                     <td><?=$fuzzy[$key]?></td>
                </tr>
                <?php endforeach?>
            </table>            
        </div>
    </div>  

    <?php if($itemset>1):
        $ass_key = ass_key($large_candicate, $com_category);
        $ass_qty = get_ass_qty($ass_key, $data);
        $confidence = get_confidence($ass_key, $ass_qty, $large_candicate);
        
        //echo '<pre>' . print_r($com_category, 1) .'</pre>';
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><a data-toggle="collapse" href="#AS_<?=$itemset?>">Aturan Asosiasi <?=$itemset?>-itemset</a></h3>
        </div>
        <div class="table-responsive collapse" id="AS_<?=$itemset?>">

        <div id="container<?=$itemset?>" style=" height: 500px; margin: 0 auto"></div>
            <table class="table table-bordered table-striped">
            <thead><tr>
                    <th>Rule</th>
                    <th>Support</th>
                    <th colspan="2">Confidence</th>
                    <th>Lift Ratio</th>
                    <!-- <th>Fuzzy Set</th> -->
                </tr></thead>  
                <!-- <?php
                // $nilaisupp = array();
                // $nilaiconfi = array();
                // $nilailiftratio = array();
                // $aturankiri = array();
                // $aturankanan = array();
                ?>        -->      
                <?php 
                foreach($ass_key as $key => $val):
                $simpanan[$itemset]['rules'][$key] = implode(', ', $val['left'])." maka ".$val['right'];
                $simpanan[$itemset]['support'][$key] = round($support[$val['index']] * 100, 2);
                $simpanan[$itemset]['confidence'][$key] = round($confidence[$key] * 100, 2);
                $simpanan[$itemset]['lift'][$key] = round($confidence[$key]/($min_confidence/100), 2);
                ?>
                
             <tbody>
                <tr class="<?=round($confidence[$key]/($min_confidence/100), 2) > 0.9 ? '' : 'hide'?>">
                    <td>Jika <strong><?=implode(' , ', $val['left'])?></strong> maka <strong><?=$val['right']?></strong>  </td>
                    <td><?=round($support[$val['index']] * 100, 2)?>%</td>
                    <td><?=round($confidence[$key] * 100, 2)?>%</td>
                    <td><?=$large_candicate[$val['index']]?>/<?=$ass_qty[$key]?></td>
                     <td><?=round($confidence[$key]/($min_confidence/100), 2)?></td>
                    <!-- <td><?=$fuzzy[$key]?></td> -->
                </tr>
                </tbody>
           
                
                    <?php endforeach?>
            </table>
          
        </div>
    </div>
    
    <?php endif; ?>
    <?php
        $arr_kategory = filter_category($large_candicate, $com_category);
        $itemset++; 
        if(count($large_candicate)<1)
            $stop = true;
        if($itemset > count($kategori))
            $stop = true;

       // $gagal = false;
    }else{
        $com_category = $temp_com_category;
        $large_candicate = $temp_large_candidate;
        $support = $temp_support;
        // $itemset--;   
        $stop = true;     

        //$gagal = true;
    };       
    //if($itemset>3)
        //$stop = true;
    ?>
<?php endwhile;?>
<button id="myBtn" class="btn btn-primary" style="padding-left: 100; padding-right: 100">Kesimpulan</button>


</div>
<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <table class="table table-bordered table-striped">
        <thead><tr>
            <th>KESIMPULAN 
            </tr></thead>  
    <?php
    foreach($ass_key as $key => $val):

    ?>
        <tbody>
            <tr class="<?=round($confidence[$key]/($min_confidence/100), 2) > 0.9 ? '' : 'hide'?>">
                <td>Jika terjadi kecelakaan dengan kriteria <strong><?=implode(' , ', $val['left'])?></strong> maka berkaitan dengan <strong><?=$val['right']?></strong>  </td>
                
                    <!-- <td><?=$fuzzy[$key]?></td> -->
                </tr>
                </tbody>
    <?php endforeach?>
    </table>
  </div>

</div>
<?php for($i = 2; $i < sizeof($simpanan)+3;$i++) { ?>
<script>


var chart = new CanvasJS.Chart("container<?=$i?>", {
    theme: "light2",
    animationEnabled: true,
    animationEnabled: true,
    exportEnabled: true,
    //theme: "light2",
    width: 1100,
    title:{
        text: "Aturan Asosiasi <?=$i?> -itemset",
        fontFamily : "Tahoma",
    },  

    axisX: {
        title: "Rules",
        titleFontColor: "#4F81BC",
        lineColor: "#4F81BC",
        labelFontColor: "#4F81BC",
        tickColor: "#4F81BC",
        labelMaxWidth: 50,
        labelFontFamily : "Bebas Neue",
    interval: 1,
    labelFontSize: 7,
    },
    axisY: {
        title: "",
        titleFontColor: "#C0504E",
        lineColor: "#C0504E",
        labelFontColor: "#C0504E",
        tickColor: "#C0504E",
        fontFamily : "Bebas Neue",

    },  
    toolTip: {
        shared: true
    },
    legend: {
        reversed: true,
        verticalAlign: "center",
        horizontalAlign: "right",
        itemclick: toggleDataSeries
    },
    data: [{
        type: "column",
        name: "Support",
        fontFamily : "Bebas Neue",
        legendText: "Hasil Support",
        showInLegend: true, 
        indexLabelFontSize: 2,
        dataPoints:[
            <?php for($a=0; $a< sizeof($simpanan[$i]['rules']); $a++) { ?>
            { label: "<?=$simpanan[$i]['rules'][$a]?>", y: parseFloat("<?=$simpanan[$i]['support'][$a]?>") },
            <?php } ?>
        ]
    },
    {
        type: "column", 
        name: "lift",
        legendText: "Hasil lift",
        showInLegend: true,
        fontFamily : "Bebas Neue",
        indexLabelFontSize: 2,
        dataPoints:[
            <?php for($a=0; $a< sizeof($simpanan[$i]['rules']); $a++) { ?>
            { label: "<?=$simpanan[$i]['rules'][$a]?>", y: parseFloat("<?=$simpanan[$i]['lift'][$a]?>") },
            <?php } ?>
        ]
    },
    {
        type: "column", 
        name: "confidence",
        legendText: "Hasil confidence",
        showInLegend: true,
        fontFamily : "Bebas Neue",
        indexLabelFontSize: 2,
        dataPoints:[
            <?php for($a=0; $a< sizeof($simpanan[$i]['rules']); $a++) { ?>
            { label: "<?=$simpanan[$i]['rules'][$a]?>", y: parseFloat("<?=$simpanan[$i]['confidence'][$a]?>") },
            <?php } ?>
        ]
    }]
});
chart.render();



function toggleDataSeries(e) {
    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    } else {
        e.dataSeries.visible = true;
    }
    e.chart.render();
}


</script>
<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

<?php } ?>


    

