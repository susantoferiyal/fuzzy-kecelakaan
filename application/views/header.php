<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/highchartss-custom.src.js"></script>
<script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/data.js"></script>
<script src="<?php echo base_url(); ?>assets/js/exporting.js"></script>
<script src="<?php echo base_url(); ?>assets/js/export-data.js"></script> -->
<script src="<?php echo base_url(); ?>assets/canvasjs.min.js"></script>


<?php
//if(!$this->session->has_userdata('login'))
    //redirect('user/login');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="icon" href="<?=base_url('favicon.ico')?>"/>
    <title>FUZZY ASSOCIATION RULES</title>
    <link href="<?=base_url('assets/css/yeti-bootstrap.min.css')?>" rel="stylesheet"/>  
    <link href="<?=base_url('assets/css/jquery.dataTables.min.css')?>" rel="stylesheet"/>    
    <link href="<?=base_url('assets/css/general.css')?>" rel="stylesheet"/>
    
    <script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>  
    <script src="<?=base_url('assets/js/jquery.dataTables.min.js')?>"></script> 

    <script type="text/javascript">    
    $(document).ready(function(){        
        $('.datatable').DataTable({
            pageLength: 10
        });
    });
    </script>          
  </head>
  <body>
    <nav class="navbar navbar-primary navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <CENTER>
          <div class="page-logo">
          <img src="<?php echo base_url()?>/assets/image/logo2.png" width="140px" height="" alt="logo" class="logo-default">
          <img src="<?php echo base_url()?>/assets/image/pol.png" width="100px" height="" alt="pol" class="logo-default">
          </div>
          <br>
          <a class="navbar-brand" style="text-align: center;" href="<?=site_url()?>"><b>ANALISIS FAKTOR PENYEBAB KECELAKAAN MENGGUNAKAN METODE FUZZY ASSOCIATION RULES</b></a>
          </CENTER>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          <?php if($this->session->userdata('login')):?>                        
            <li><a href="<?=site_url('data')?>"><span class="glyphicon glyphicon-th-list"></span> Data</a></li>                        
            <li><a href="<?=site_url('apriori')?>"><span class="glyphicon glyphicon-signal"></span> Hitung</a></li>                                     
            <li><a href="<?=site_url('user/password')?>"><span class="glyphicon glyphicon-lock"></span> Password</a></li>
            <li><a href="<?=site_url('user/logout')?>"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
            <?php else:?>
            <li><a href="<?=site_url('home/tentang')?>"><span class="glyphicon glyphicon-question-sign"></span> Tentang</a></li>
            <li><a href="<?=site_url('user/login')?>"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
           <?php endif?>      
          </ul>          
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="container" style="min-height: 400px;">
        <div class="page-header">
            <h1><?=$title?></h1>
        </div>