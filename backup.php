<?php
error_reporting(E_ALL);
error_reporting();
ini_set('display_errors', 1);
ini_set('max_execution_time', 60 * 10);
ini_set('memory_limit', '256M');

function get_paging($modul, $total, $per_page){
    $CI =&get_instance();
    
    $config['base_url'] = site_url("$modul?");
    $config['total_rows'] = $total;
    $config['per_page'] = $per_page;
    $config['page_query_string'] = TRUE;
    $config['reuse_query_string'] = TRUE;
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a>';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $CI->pagination->initialize($config);
    
    return '<ul class="pagination pagination-sm  pull-right">'.$CI->pagination->create_links().'</ul>';
}
function set_msg($msg, $type='success'){
    $CI =& get_instance();
    $CI->session->set_userdata('MSG', array('msg' => $msg, 'type' => $type));    
}
function show_msg(){
    $CI =& get_instance();
    $MSG = $CI->session->userdata('MSG');
    if($MSG){
        $CI->session->set_userdata('MSG', array());
        return '<div class="alert alert-'. $MSG['type'].'" alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>'.$MSG['msg'].'</div>';
    }    
}

function get_match($needed, $data){
    $matches = 0;
    foreach($data as $k => $v){
        $counter = 0;            
        foreach($v as $a => $b){
            if(in_array($b, $needed))
                $counter++;
        }
        if($counter >= count($needed))
            $matches++;
    }
    return $matches;
}

function get_match_result($needed, $data){
    $matches = 0;
    foreach($data as $k => $v){
        $counter = 0;            
        foreach($v as $a => $b){
            if($b == $needed)
                $counter++;
        }
        if($counter >= count($needed))
            $matches++;
    }
    return $matches;
}

function get_com_category($arr_kategory, $itemset){  
    return getCombinations($arr_kategory, $itemset)      ;    
}

function getCombinations($base,$n){
    $baselen = count($base);
    if($baselen == 0){
        return;
    }
    if($n == 1){
        $return = array();
        foreach($base as $b){
            $return[] = array($b);
        }
        return $return;
    }else{
        //get one level lower combinations
        $oneLevelLower = getCombinations($base,$n-1);

        //echo '<pre>'.$n.': '.print_r($oneLevelLower, 1).'</pre>';

        //for every one level lower combinations add one element to them that the last element of a combination is preceeded by the element which follows it in base array if there is none, does not add
        $newCombs = array();
        foreach((array)$oneLevelLower as $oll){
            $lastEl = $oll[$n-2];
            $found = false;
            foreach($base as  $key => $b){
                if($b == $lastEl){
                    $found = true;
                    continue;
                    //last element found
                }
                if($found == true){
                    //add to combinations with last element
                    if($key < $baselen){
                        $tmp = $oll;
                        $newCombination = array_slice($tmp,0);
                        $newCombination[]=$b;
                        $newCombs[] = array_slice($newCombination,0);
                    }
                }
            }
        }
    }
    return $newCombs;
}

function get_candidate($data, $com_category){
    $arr = array();    
    foreach($com_category as $key => $val){        
        $arr[$key] = get_match($val, $data);
    }
    return $arr;    
}
function get_fuzzy($candicate, $total){
    $arr = array();
    foreach($candicate as $key => $val){
        $arr[$key] = groupFuzzy($val / $total);
    }
    return $arr;
}
function get_support($candicate, $total){
    $arr = array();
    foreach($candicate as $key => $val){
        $arr[$key] = $val / $total;
    }
    return $arr;
}
function filter_candicate($candicate, $support, $min_support, $fuzzySet){
    $arr = array();
    foreach($candicate as $key => $val){
        if($support[$key]>=$min_support
            || filter_fuzzy($fuzzySet[$key], $min_support)
            ) {
            $arr[$key] = $val;
        }
    }
    return $arr;
}

function filter_category($l_can, $com_category){
    $arr = array();
    foreach($l_can as $key => $val){
        foreach($com_category[$key] as $v){
            $arr[$v] = $v;
        }
    }
    //echo '<pre>'. print_r($arr, 1). '</pre>';
    return array_values($arr);
}

function ass_key($large_candicate, $com_category){
    $status = ['LUKA RINGAN', 'MENINGGAL DUNIA'];
    $category = filter_category($large_candicate, $com_category);
    
    $arr = array();
    foreach($large_candicate as $key => $val){
        $arr[$key] = $com_category[$key];
    }
    
    $arr2 = array();
    $no = 0;
    foreach($arr as $key => $val){
        for ($i = 0; $i < sizeof($status); $i++) {
            // for($a = 0; $a < count($val); $a++){                                       
                $arr2[$no]['left'] = $val;
                $arr2[$no]['right'] = $val[0];
                $arr2[$no]['result'] = $status[$i];
                unset($arr2[$no]['left'][0]); 
                   
                $arr2[$no]['index'] = $key;
                $no++;
            // }
        }                
    }
    
    $arr = $arr2;
    
    return $arr;
}
/*
function get_ass_name($keys, $KATEGORI){
    $arr = array();
    foreach($keys as $key){
        $arr[] = $KATEGORI[$key]->nama_kategori;
    }
    return implode(', ', $arr);
}*/

function get_ass_qty($ass_key, $data){
    //echo '<pre>' . print_r($ass_key, 1) . '</pre>';

    $arr = array();
    foreach($ass_key as $key => $val){
        //$arr[$key] = get_match(array($val['right']), $data);
        $arr[$key] = get_match($val['left'], $data);
    }
    return $arr;
}

function get_ass_qty_result($ass_key, $data){
    // echo '<pre>' . print_r($ass_key, 1) . '</pre>';

    $arr = array();
    foreach($ass_key as $key => $val){
        //$arr[$key] = get_match(array($val['right']), $data);
        $arr[$key] = get_match_result($val['result'], $data);
    }
    return $arr;
}

function get_confidence($ass_key, $ass_qty, $large_candicate){
    $arr = array();
    foreach($ass_key as $key => $val){
        $arr[$key] = $large_candicate[$val['index']] / $ass_qty[$key];
    }
    return $arr;
}

function convert_data($data, $kategori){
    $CI =&get_instance();
    $arr = array();
    $arr_kategori = array();
    foreach($kategori as $row){
        $arr_kategori[] = strtolower($row->nama_kategori);
    }
    //echo '<pre>' . print_r($arr_kategori, 1) . '</pre>';
    foreach($data as $row){
        $arr[$row->id_data] = array();
        foreach($arr_kategori as $key => $val){
            $nilai = 0;
            if(substr($row->nama_data, $key, 1)==1){
                $arr[$row->id_data][] = $val;
                $nilai = 1;
            }
            //$key = $key + 1;
            //$CI->db->query("REPLACE INTO tb_relasi (id_data, id_kategori, nilai) VALUES ('$row->id_data', '$key', '$nilai')");
        }        
    }              
    //echo '<pre>' . print_r($arr, 1) . '</pre>';  
    return $arr;
}









function load_view($view, $data = array())
{
    $CI =&get_instance();
    $CI->load->view('header', $data);
    $CI->load->view($view, $data);
    $CI->load->view('footer', $data);    
}


function groupFuzzy($value) {
    $range = '';
    if ($value >= 0.01 && $value < 0.2) {
        $range .= 'very low';
    } 

    if ($value >= 0.16 && $value <= 0.30) {
        if ($range != '')
            $range .= ',';
        $range .= 'low';
    }

    if ($value >= 0.24 && $value <= 0.47) {
        if ($range != '')
            $range .= ',';
        $range .= 'medium';
    }

    if ($value >= 0.38 && $value <= 0.64) {
        if ($range != '')
            $range .= ',';
        $range .= 'high';
    }

    if ($value >= 0.6 && $value <= 1) {
        if ($range != '')
            $range .= ',';
        $range .= 'very high';
    }

    return $range;
}

function filter_fuzzy($value, $minValue) {
    $pass = false;
    $minGroup = groupFuzzy($minValue);
    $keys = explode(',', $value);
        $minKeys = explode(',', $minGroup);
        foreach ($keys as $key) {
            foreach ($minKeys as $minKey) {
                if ($key == $minKey) {
                    $pass = true;
                    break;
                }
            }
        }
    return $pass;
}